﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PigBombs {
	public class BombComponent : MonoBehaviour {
		[SerializeField]
		private BoxCollider2D _collider1, _collider2;
		[SerializeField]
		private float _liveTime;
		[SerializeField]
		private LayerMask _StoneLayer;

		private void Start() {
			StartCoroutine(OnStart());
		}

		private IEnumerator OnStart() {
			yield return new WaitForSeconds(_liveTime);

			if(!Physics2D.OverlapCircle(new Vector2(transform.position.x + 1, transform.position.y), 0.3f, _StoneLayer)) _collider1.enabled = true;
			if(!Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y + 1), 0.3f, _StoneLayer)) _collider2.enabled = true;
			
			Destroy(gameObject, 0.2f);
		}

		private void OnTriggerEnter2D(Collider2D collision) {
			var bot = collision.GetComponent<BotComponent>();
			var player = collision.GetComponent<PlayerComponent>();

			if(bot != null && bot.BotState != BotState.Dirty) {
				bot.StartDirt();
				GameObject.FindObjectOfType<GameManager>().CheckWin();
			}

			if(player != null) {
				player.StartStun();
			}
		}
	}
}