﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PigBombs {
	public class PlayerComponent : MonoBehaviour {
		[SerializeField]
		private Joystick _joystick;
		[SerializeField]
		private float _moveSpeed;
		[SerializeField]
		private Rigidbody2D _rb;
		[SerializeField]
		private SpriteRenderer _sr;
		[SerializeField]
		private Sprite _leftPig, _rightPig, _upPig, _downPig;
		[SerializeField]
		private float _stunTime;
		[SerializeField]
		private GameManager _gameManager;

		private bool _stunned;

		private void FixedUpdate() {
			if(_stunned) return;

			var moveHor = _joystick.Horizontal;
			var moveVer = _joystick.Vertical;

			DetermineDirection(moveHor, moveVer);

			_rb.velocity = new Vector2(moveHor * _moveSpeed, moveVer * _moveSpeed);
		}

		private void DetermineDirection(float moveHor, float moveVer) {
			if(moveHor >= 0 && moveVer >= 0 && moveHor > moveVer) {
				_sr.sprite = _rightPig;
			}
			else if(moveHor >= 0 && moveVer >= 0 && moveVer > moveHor) {
				_sr.sprite = _upPig;
			}
			else if(moveHor <= 0 && moveVer <= 0 && moveHor < moveVer) {
				_sr.sprite = _leftPig;
			}
			else if(moveHor <= 0 && moveVer <= 0 && moveVer < moveHor) {
				_sr.sprite = _downPig;
			}
			else if(moveHor >= 0 && moveVer <= 0 && moveHor > -moveVer) {
				_sr.sprite = _rightPig;
			}
			else if(moveHor >= 0 && moveVer <= 0 && moveHor < -moveVer) {
				_sr.sprite = _downPig;
			}
			else if(moveHor <= 0 && moveVer >= 0 && -moveHor < moveVer) {
				_sr.sprite = _upPig;
			}
			else if(moveHor <= 0 && moveVer >= 0 && -moveHor > moveVer) {
				_sr.sprite = _leftPig;
			}
		}

		public void StartStun() {
			StartCoroutine(Stunned());
			StartCoroutine(Tremor());
		}

		private IEnumerator Stunned() {
			_stunned = true;
			yield return new WaitForSeconds(_stunTime);
			_stunned = false;
		}

		private IEnumerator Tremor() {
			var timer = 3f;
			while(timer >= 0f) {
				timer -= Time.deltaTime;

				transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(-5, 5));

				yield return null;
			}
			transform.rotation = new Quaternion();
		}

		private void OnCollisionEnter2D(Collision2D collision) {
			var bot = collision.gameObject.GetComponent<BotComponent>();

			if(bot != null) {
				_gameManager.GameOver();
			}
		}
	}
}