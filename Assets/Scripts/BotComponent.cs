﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PigBombs {
	public class BotComponent : MonoBehaviour {
		[SerializeField]
		private float _dirtyTime;
		[SerializeField]
		private GameObject _player;
		[SerializeField]
		private float _moveSpeed;
		[SerializeField]
		private Rigidbody2D _rb;
		[SerializeField]
		private SpriteRenderer _sr;
		[SerializeField]
		private Sprite[] _defaultSprites;
		[SerializeField]
		private Sprite[] _andrySprites;
		[SerializeField]
		private Sprite[] _dirtySprites;
		[SerializeField]
		private CircleCollider2D _circleCollider;

		private List<Vector2> _pathToPlayer = new List<Vector2>();
		private PathFinder _pathFinder;
		private bool _isMoving;
		private bool _isDirty;

		public BotState BotState { get; set; }

		private void Start() {
			BotState = BotState.Default;

			if(_player != null) {
				_pathFinder = GetComponent<PathFinder>();
				_pathToPlayer = _pathFinder.GetPath(_player.transform.position);
				_isMoving = true;
			}

			StartCoroutine(ResearchPath());
		}

		private void Update() {
			if(_isMoving && !_isDirty) BotState = BotState.Angry;
			if(!_isMoving && !_isDirty) BotState = BotState.Default;
			if(_isDirty) BotState = BotState.Dirty;
		}

		private void FixedUpdate() {
			if(_player == null || _isDirty) return;

			if(_pathToPlayer.Count == 0 && Vector2.Distance(transform.position, _player.transform.position) > 0.5f) {
				_pathToPlayer = _pathFinder.GetPath(_player.transform.position);
				_isMoving = true;
			}

			if(_pathToPlayer.Count == 0) return;

			if(_isMoving) {
				if(Vector2.Distance(transform.position, _pathToPlayer[_pathToPlayer.Count - 1]) > 0.3f) {
					var heading = _pathToPlayer[_pathToPlayer.Count - 1] - new Vector2(transform.position.x, transform.position.y);
					_rb.velocity = heading / heading.magnitude * _moveSpeed;
					DetermineDirection(_rb.velocity.x, _rb.velocity.y);
				}
				else {
					_isMoving = false;
				}
			}
			else {
				_pathToPlayer = _pathFinder.GetPath(_player.transform.position);
				_isMoving = true;
			}
		}

		private IEnumerator ResearchPath() {
			var timer = 0.5f;

			while(true) {
				timer -= Time.deltaTime;

				if(timer <= 0) {
					timer = 0.5f;

					_pathToPlayer = _pathFinder.GetPath(_player.transform.position);
				}

				yield return null;
			}
		}

		private void DetermineDirection(float moveHor, float moveVer) {
			if(moveHor >= 0 && moveVer >= 0 && moveHor > moveVer) {
				_sr.sprite = GetSprite(BotDirection.Right);
			}
			else if(moveHor >= 0 && moveVer >= 0 && moveVer > moveHor) {
				_sr.sprite = GetSprite(BotDirection.Up);
			}
			else if(moveHor <= 0 && moveVer <= 0 && moveHor < moveVer) {
				_sr.sprite = GetSprite(BotDirection.Left);
			}
			else if(moveHor <= 0 && moveVer <= 0 && moveVer < moveHor) {
				_sr.sprite = GetSprite(BotDirection.Down);
			}
			else if(moveHor >= 0 && moveVer <= 0 && moveHor > -moveVer) {
				_sr.sprite = GetSprite(BotDirection.Right);
			}
			else if(moveHor >= 0 && moveVer <= 0 && moveHor < -moveVer) {
				_sr.sprite = GetSprite(BotDirection.Down);
			}
			else if(moveHor <= 0 && moveVer >= 0 && -moveHor < moveVer) {
				_sr.sprite = GetSprite(BotDirection.Up);
			}
			else if(moveHor <= 0 && moveVer >= 0 && -moveHor > moveVer) {
				_sr.sprite = GetSprite(BotDirection.Left);
			}
		}

		private Sprite GetSprite(BotDirection botDirection) {
			Sprite sprite = null;

			switch(botDirection) {
				case BotDirection.Right:
					switch(BotState) {
						case BotState.Default:
							sprite = _defaultSprites[0];
							break;
						case BotState.Angry:
							sprite = _andrySprites[0];
							break;
						case BotState.Dirty:
							sprite = _dirtySprites[0];
							break;
					}
					break;
				case BotDirection.Left:
					switch(BotState) {
						case BotState.Default:
							sprite = _defaultSprites[1];
							break;
						case BotState.Angry:
							sprite = _andrySprites[1];
							break;
						case BotState.Dirty:
							sprite = _dirtySprites[1];
							break;
					}
					break;
				case BotDirection.Down:
					switch(BotState) {
						case BotState.Default:
							sprite = _defaultSprites[2];
							break;
						case BotState.Angry:
							sprite = _andrySprites[2];
							break;
						case BotState.Dirty:
							sprite = _dirtySprites[2];
							break;
					}
					break;
				case BotDirection.Up:
					switch(BotState) {
						case BotState.Default:
							sprite = _defaultSprites[3];
							break;
						case BotState.Angry:
							sprite = _andrySprites[3];
							break;
						case BotState.Dirty:
							sprite = _dirtySprites[3];
							break;
					}
					break;
			}

			return sprite;
		}

		public void StartDirt() {
			StartCoroutine(Dirt());
		}

		private IEnumerator Dirt() {
			_isDirty = true;
			BotState = BotState.Dirty;
			_circleCollider.isTrigger = true;
			DetermineDirection(_rb.velocity.x, _rb.velocity.y);
			_rb.velocity = new Vector2();
			yield return new WaitForSeconds(_dirtyTime);
			_isDirty = false;
			_circleCollider.isTrigger = false;
		}
	}

	public enum BotState : byte {
		Default,
		Angry,
		Dirty
	}

	public enum BotDirection : byte {
		Left,
		Right,
		Up,
		Down
	}
}