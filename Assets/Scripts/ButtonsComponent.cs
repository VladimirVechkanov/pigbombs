﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

namespace PigBombs {
	public class ButtonsComponent : MonoBehaviour {
		[SerializeField]
		private Transform _player;
		[SerializeField]
		private GameObject _bombPrefab;

		private float _timer;

		private void Update() {
			if(_timer > 0f) {
				_timer -= Time.deltaTime;
			}
		}

		public void BombButton() {
			if(_timer > 0f) return;

			var bomb = Instantiate(_bombPrefab, _player.position, new Quaternion());

			_timer = 3f;
		}

		public void Restart() {
			Time.timeScale = 1;
			SceneManager.LoadScene("GameScene");
		}
	}
}