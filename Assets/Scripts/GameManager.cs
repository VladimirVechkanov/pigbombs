﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PigBombs {
	public class GameManager : MonoBehaviour {
		[SerializeField]
		private GameObject _gameOver;
		[SerializeField]
		private Text _gameOverText;

		public void GameOver() {
			_gameOver.SetActive(true);
			Time.timeScale = 0;
		}

		private void GameWin() {
			_gameOverText.text = "You Win";
			_gameOver.SetActive(true);
			Time.timeScale = 0;
		}

		public void CheckWin() {
			var bots = GameObject.FindObjectsOfType<BotComponent>();
			var dirtyBots = bots.Where(x => x.BotState == BotState.Dirty).ToArray();

			if(bots.Length == dirtyBots.Length) {
				GameWin();
			}
		}
	}
}